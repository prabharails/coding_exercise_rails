class CreatePriceLists < ActiveRecord::Migration
  def change
    create_table :price_lists do |t|
      t.text :prices
      t.datetime :published_date

      t.timestamps null: false
    end
  end
end
