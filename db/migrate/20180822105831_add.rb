class Add < ActiveRecord::Migration
  def change
   create_table :currencies do |t|
    t.string :name
   end

   currency_name = ["USD","JPY","BGN","CZK","DKK","GBP","HUF","PLN","RON","SEK", "CHF" ,"ISK","NOK","HRK","RUB","TRY","AUD","BRL","CAD","CNY","HKD",
                  "IDR", "ILS","INR","KRW","MXN","MYR","NZD","PHP","SGD","THB","ZAR"]  
   currency_name.each do |c_name|
     Currency.create(name: c_name)
   end
  end
end
