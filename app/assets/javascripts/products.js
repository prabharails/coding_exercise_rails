 $(function () {
     $('#Change_Currency').on('change', function(){
      var productsFlag = $("article").length
      var currency = $(this).val();
      var base_url = '/get_exchange_values/';
      var url  = productsFlag.length == 1 ?  base_url + $('.text-data').data("prod") : base_url
      $.ajax({
        datatype: 'html',
        url: url,
        type: 'get',
        async: true,
        data: {currency},
        success: function(data) {
        // $("#prod_cur_"+prod_id+"").html(data+Status)
          if(data){
           data.map((prod) => $("article").find("#prod_cur_"+prod.id).html("<strong> "+prod.currency+"&nbsp;<label>"+currency+"</label>&nbsp;&nbsp;&nbsp;<a href='/products/'"+prod.id+">Show more</a> </strong> "))
          }else{
            alert("There was an error while converting currency values.")
          }
        }
       });
     });
    });