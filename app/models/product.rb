class Product < ActiveRecord::Base
 require 'httpclient'
 require 'active_support/core_ext'


  def price
    price_in_cents / 100.0
  end

  def self.get_currency_rate(status,prod)
    price_list = PriceList.last
    base_value = prod.price_in_cents/100.0

    if (price_list.blank? || (!price_list.blank? && price_list.published_date < Date.today))
      res = HTTPClient.get("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml")
      hash = Hash.from_xml(res.body)["Envelope"]["Cube"]["Cube"][0]
      price_list = PriceList.create({:prices => hash , published_date: Time.now.to_date})
    end
    a =  price_list.prices["Cube"].select{|k,v| k["currency"] == status}[0].values[1]
    result = "%0.2f" % [a.to_f * base_value]
  end

end
